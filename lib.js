const SLOW_FACTOR = 3;

function buildLargeObject() {
	const testMe = {};
	for(let i=0; i<100; i++) {
		const key = `a_basdasdsad_${i}`;
		testMe[key] = {};
		for(let j=0; j<100; j++) {
			const subkey = `basdasd_${i}${j}`;
			testMe[key][subkey] = 'asdasdasd';
		}
	}
	return testMe;
}

function slowmeDown() {
	for(let i=0; i<SLOW_FACTOR; i++) {
		for(let j=0; j<SLOW_FACTOR; j++) {
			const x = Math.sqrt(i*j);
			if (x > Math.sqrt(j*i)) {
				console.log('This should never be printed');
			}
		}
	}
}

module.exports.buildLargeObject = buildLargeObject;
module.exports.slowmeDown = slowmeDown;