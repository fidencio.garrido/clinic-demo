# Node profiling demo

## Requirements

- Node 10

## Profile for slow code

### Step 1. Start your service with node clinic

Start node clinic
```sh
npx clinic doctor -- node index.js
```

### Step 2. Profile targetting some slow code

Run autocannon
```sh
npx autocannon http://localhost:3000
```

### Step 3. Stop the service from step 1

Press Ctrl-C. Then wait for the clinic report to be generated.

### Step 4. Start clinic flame

Run flame
```sh
npx clinic flame -- node index.js
```

Run again autocannon (repeat step 3).

### Step 5. Stop the service from step 5

Press Ctrl-C. Then wait for the clinic report to be generated.

## Profile for slow remote code

### Step 1. Start your service with node clinic

Start node clinic
```sh
npx clinic doctor -- node index.js
```

### Step 2. Start "remote" service
```sh
node remote.js
```

### Step 3. Profile targetting some slow code

Run autocannon
```sh
npx autocannon http://localhost:3000
```

### Step 4. Stop the service from step 1

Press Ctrl-C. Then wait for the clinic report to be generated.

### Step 5. Start clinic flame

Run bubbleprof
```sh
npx clinic bubbleprof -- node index.js
```

Run again autocannon (repeat step 3).

### Step 6. Stop the service from step 5

Press Ctrl-C. Then wait for the clinic report to be generated.
