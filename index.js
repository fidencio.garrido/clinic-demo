const express = require('express');
const request = require('request');
const promclient = require('prom-client');
const { slowmeDown, buildLargeObject } = require('./lib');

const app = express();
const PORT = process.env.PORT || 3000;
const small = {something: 'small is ok', number: 33, isTrue: false, name: 'fido'};
const collectDefaultMetrics = promclient.collectDefaultMetrics;
const Registry = promclient.Registry;
const register = new Registry();
collectDefaultMetrics({ register });

const size = 'smallz';
const jsonobject = (size === 'small') ? small : buildLargeObject();
const stringified = JSON.stringify(jsonobject);
const buffered = Buffer.from(stringified);
const h = {'content-type': 'application/json'};

app.disable('etag');
app.disable('x-powered-by');

app.get('/prometheus', (_, res) => {
	res.send(register.metrics());
});

app.get('/doc/j', (_, res) => {
	res.send(jsonobject);
});

app.get('/doc/s', (_, res) => {
	res.set(h).send(stringified);
});

app.get('/doc/b', (_, res) => {
	res.set(h).send(buffered);
});

// Slow sync operation
app.get('/', (_, res) => {
	slowmeDown();
	res.send('hello world!');
});

app.get('/slowdownstream', (_, res) => {
	request.get('http://localhost:3001', (err, _) => {
		if(err) {
			res.status(500).send(err);
		} else {
			res.send({});
		}
	});
});

app.listen(PORT, () => {
	console.log(`app_port=${PORT}`);
});