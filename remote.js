const express = require('express');

const REMOTE_PORT = 3001;
const app = express();

const slowMe = false;

app.get('/', (req, res) => {
	if (slowMe === true) {
		setTimeout(()=> {
			res.send({});
		}, 3000);
	} else {
		res.send({});
	}
});

app.listen(REMOTE_PORT, () => {
	console.log(`app_port=${REMOTE_PORT}`);
});